"""
Create an IMOS-like SQL database

M.Rayson
UWA
July 2016
"""

import os
import sqlite3
import pandas as pd


# Dictionary to convert from IMOS to SQL types
datadict = {
    'Text':'TEXT',\
    'Date/Time':'TEXT',\
    'Number':'REAL',\
    'Memo':'TEXT',\
    'Yes/No':'TEXT',\
    }


##########
# Sample inputs
dbfile = 'test/test-imos.db'
templatefile = 'src/imos-sql-template.csv'
##########


#######
# Load the CSV file and create a dictionary with all of the tables and fields
df = pd.read_csv(templatefile, delimiter='\t')

# Use the unique tables
tables = [ii for ii in df]

sql = {}
for name, field, dtype in zip(df['Table Name'], df['Field Name'], df['Data Type']):
    if not sql.has_key(name):
        # Initialise the table
        sql.update({name:{ 'FieldName':[], 'DataType':[] }})

    sql[name]['FieldName'].append(field)
    sql[name]['DataType'].append(datadict[dtype])

#######
# Create the dbfile
if os.path.exists(dbfile):
    print 'Deleting old db file.'
    os.unlink(dbfile)

conn = sqlite3.connect(dbfile)
cursor = conn.cursor()

#######
# Create each table
for name in sql.keys():
    # Generate the SQL command
    createstr = 'CREATE TABLE `%s` ('%name
    ii = 0
    for field, dtype in zip(sql[name]['FieldName'], sql[name]['DataType']):
        if ii > 0:
            createstr += ', '
        createstr += '`%s` %s'%(field, dtype)
        ii+=1

    createstr += ');'

    cursor.execute(createstr)

#######
# Commit the changes and close it down
conn.commit()
cursor.close()
conn.close()

